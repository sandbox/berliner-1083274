// $Id$

Apply Changes Module

Description
===========
This module provides an "apply changes" functionality to node edit forms,
meaning you can save the changes you have made to the current node, without
leaving the edit screen. This comes in two possible flavours, either as a
seperate button "Apply changes" which will be right beside the usual "Save"
button, or as an override to the save button, so that you get the functionality
without more buttons.

Installation
===========

1. Unpack the Apply Changes module folder and contents in the
   appropriate modules directory of your Drupal installation. This is probably
   sites/all/modules/ or sites/MYSITE/modules
2. Enable the module in the administration section of your site:
   admin/build/modules
3. Now go to every content type for which you want to enable an "apply changes"
   button, edit the content type and check the settings under the "submission
   form settings".

No permissions, no seperate configuration beside the node type specific
settings. Everything should work right out of the box.

Usage
===========
Simple as it is, just hit the button.